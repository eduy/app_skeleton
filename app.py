from flask import (
    Flask,
    url_for,
    request,
    render_template
)
from lib.models import toDict, Feed
from lib.database import db_session

app = Flask(__name__)
#app.debug = True

@app.errorhandler(404)
def not_found():
    return render_template('404.html'), 404

@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500

@app.route('/')
def home():
	return render_template('home.html')

@app.route('/feeds/')
@app.route('/feeds/<int:uid>')
@app.route('/feeds/<int:uid>/<task>', methods=['GET','POST'])
def feeds(uid=None, task=None):

	if uid is None:
		#print "display list of feeds"
		feeds = db_session.query(Feed).all()
		return render_template('feeds.html', feeds=feeds)
	
	if task == 'update':
		#print "task is update bhru"
		feed=request.form
		return render_template('update.html', feed=feed)
	
	else:
		feed = toDict(db_session.query(Feed).filter(Feed.id==uid).one())
	
	
	return render_template('edit_form.html' if task=='edit' else 'feed_details.html' , feed=feed)


if __name__ == '__main__':
	app.run()